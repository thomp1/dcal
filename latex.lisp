(in-package :dcal)
;;;
;;; latex.lisp - generate LaTeX markup for weekly schedule 
;;;
(defmethod days (stream (format (eql :latex))
		 &key date nubsp tags)
  (let (
	;; desired is 19 (7 PM)
	(desired-page-bottom-y (m:m* *hour-height* (- *day-end-time* *picture-y-top*))))
    (let ((actual-page-bottom-y (if (m:m> desired-page-bottom-y *physical-page-bottom-y*)
				    *physical-page-bottom-y*
				    desired-page-bottom-y))
	  (start-day (car *day-range*))
	  (last-day (cdr *day-range*)))      
      (do ((day start-day (1+ day)))
	  ((> day last-day))
	(day day stream :actual-page-bottom-y actual-page-bottom-y :nubsp nubsp :tags tags))
      (dltx:comment "Draw *final* vertical day divider" :stream stream)
      (day-vertical-divider last-day actual-page-bottom-y stream))))

(defun day-vertical-divider (day-corner-x actual-page-bottom-y stream)
  (format stream "
% Draw vertical day divider
    \\draw (~A,~A) -- (~A,~A);
"
	  day-corner-x
	  *day-start-time*
	  day-corner-x
	  (+ *picture-y-top* (m:m/ actual-page-bottom-y *hour-height*))))

(defun nub (x &optional stream)
  (format stream "\\filldraw[fill=black] (~A,\\time) circle (0.1em);" x)
  ;;(format nil "\\draw[color=black, thick] (~A,\\time) -- (~A,\\time);" (- x 0.1) x)
  ;;(format nil "\\node[anchor=center, shape=circle, fill=black, text width={0.001}] at (~A,\\time) {};" x)
  )

(defun times-markers (stream x &key font-size textp nubsp horiz-lines-p)
  "Proceeding along the vertical axis at x position X, add text representations of time if TEXTP is true and add 'nubs' if NUBSP is true."
  ;; Flexible time range for a daily or weekly calendar
  (let ((time-start *day-start-time*)
	(time-end *day-end-time*))
    (format stream
	    "
      % First print a list of times.
    \\foreach \\time/\\ustime in {")
    ;; output sequence a la "8/8,9/9,10/10,11/11,12/12,13/1,14/2,15/3,16/4,17/5"
    ;; TIME is a number between 0 and 24
    ;; assume 1 hour steps on calendar
    (do ((time time-start (1+ time)))
	((> time (1- time-end))
	 (format stream "~A/~A"
	      time
	      (if (> time 12)
		  (- time 12)
		  time)))
      (format stream "~A/~A,"
	      time
	      (if (> time 12)
		  (- time 12)
		  time)))
    (format stream
	    "}
{
~A
~A
~A
}
"
	    ;; add text?
	    (if textp
		(format nil "\\node[anchor=east,font=\\~A] at (~A,\\time) {\\ustime};"
			(or font-size *hour-text-font-size*) x)
		"")
	    (if nubsp
		(with-output-to-string (s) (nub x s))
		"")
	    (if horiz-lines-p
		(format nil "\\draw[color=gray!50!white, thin] (0,\\time) -- (~A,\\time);"
			(m:m->string (m:m* *day-width* 5)))
		""))
    ))
