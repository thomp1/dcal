(in-package :dcal)
;;
;; event-nodes.lisp
;;
(defun event-box-height (start end)
  "Return box height as a measure."
  (declare (special %event-box-height% %hour-height%))

  ;; machinations below define a box size based on start/end times

  ;; establish when, in hours -- relative to y-time, this thing starts and ends 
  ;; - these are floating point values (precise values, not integers...)
  (let ((start-hr (seconds-to-hours start))
	(end-hr (seconds-to-hours end)))
    (let ((hr-delta-exact (- end-hr start-hr))) 
      ;; if end > start, we assume event has meaningful length; otherwise use an arbitrary default box height... 
      (if (> end start)
	  (m:mm (* (m:m-numb %hour-height%) hr-delta-exact)
	      (m:m-unit %hour-height%))
	  %event-box-height%))))

;; Y: number corresponding to Y offset; assume height of %hour-height-units%
;; Y-TIME: universal time corresponding to Y
;; UID: a string, a unique identifier for the event

;; goal here is to (1) generate initial node and populate y slot and box-height slot and uid slot
(defun event-node03 (vevent y y-time &key uid)
  "Return a node structure. Y is a measure."
  (declare (special %hour-height% %line-width% %node-inner-sep%))
  ;; need to define here or elsewhere: start end y y-time
  (multiple-value-bind (dtstart dtend dtstart-type dtend-type)
      (vevent-dtstart-dtend vevent)
    (declare (ignore dtstart-type dtend-type))
    (let ((%event-box-height% 
	   (m:mm 0.5 (m:m-unit %hour-height%)))
	  ;; FIXME: use unique identifier associated with ical spec (see ical spec) -> use here as basis of node name
	  (uid (or uid (format nil "~A" (random 1000000)))))
      (declare (special %event-box-height%))
      (let ((box-height (event-box-height dtstart dtend))
	    ;;(fill "white")
	    (node-name (format nil "Event~A" uid))
	    ;; note that 'down' is negative on the y axis in tikz
	    (this-y (m:mm
		     (* (+ (m:m-numb y) 
			   (* -1 
			      (seconds-to-hours (- dtstart y-time))
			      (m:m-numb %hour-height%))))
		     (m:m-unit %hour-height%))))
	(make-node
	:box-height box-height
	:name node-name
	:y this-y)))))

;; FIXME:
;; PARAMS = ?
;; -> default should specify anchor=center, fill=white, opacity = 1
;; \node[anchor=center, draw, fill=white, font=\tiny, inner sep=0.045cm, opacity=1, rectangle, yshift=0.2pt] at (Event149294.north) {07:00};
(defun event-start-time-box (node params stream vevent)
  (declare (special %line-width%))
  ;; render exact start time
  (when params
    (destructuring-bind (anchor yshift)
	(cond  
	  ((member :above params)
	   (list "south west" (format nil "-~A" (m:m->string (m:m/ %line-width% 2)))))
	  ((member :center params)
	   (list "center" (format nil "~A~A~A" 
				  ;; OLD: perfectly centered vertically:
				  ;;"-" 0 (m:m-unit %line-width%)
				  ;; shift a slight smidge up
				  "+" "1" "pt"
				  )))
	  (t				;(member :below params)
	   (list "north west" (m:m->string (m:m/ %line-width% 2)))))
      (let ((start (vevent-dtstart vevent))
	    (opacity "1.0")		; or 0.7?
	    )
	
	;; full rectangle
	;; (format stream "\\node[anchor=~A, draw, fill=white, font=\\~A, inner sep=0.045cm, opacity=~A, rectangle, yshift=~A] at (~A.north) {~A};~%"
	;; 	anchor
	;; 	*event-start-time-font-size*
	;; 	opacity
	;; 	yshift
	;; 	(node-name node)
	;; 	(format nil "~2,'0',,D:~2,'0',,D" (universal-time-hr start) (universal-time-min start)))
	
	;; one-sided rectangle
	(format stream "\\node[rectL, anchor=~A, fill=white, font=\\~A, inner sep=0.045cm, opacity=~A, rectangle, yshift=~A] at (~A.north) {~A};~%"
		anchor
		*event-start-time-font-size*
		opacity
		yshift
		(node-name node)
		(format nil "~2,'0',,D:~2,'0',,D" (universal-time-hr start) (universal-time-min start)))

	))))

;; see EVENT-NODE

;; for events w/start and end datetimes, if FILL is true, draw rectangle and fill rectangle to obscure time lines
(defun render-event-box (vevent node stream &key event-box-style (fill t) (font-size "footnotesize"))
  ;; for events w/only start dates, don't render bottom line or RH line of box -> distinguish visual representation  
  (if (> (vevent-dtend vevent) (vevent-dtstart vevent))
      (cond ((eq event-box-style :thinbox)
	     (format stream  "~A (~A.north west) rectangle ++(~A,-~A);"
		     (if fill
			 "\\filldraw [fill=white]"
			 "\\draw ")
		     (node-name node) 
		     (m:m->string (node-box-width node))
		     (m:m->string (node-box-height node))))
	    (t			       ; (eq event-box-style :halfbox)
	     (format stream  "~A (~A.north west) rectangle ++(~A,-~A);"
		     (if fill
			 "\\filldraw [fill=white,color=white]"
			 "\\draw ")
		     (node-name node) 
		     (m:m->string (node-box-width node))
		     (m:m->string (node-box-height node)))
	     (format stream "\\draw [~A,~A] (~A.north west) -- ++ (~A,0);~%" 
		     *event-box-line-thickness*
		     *event-box-line-color*
		     (node-name node) 
		     (m:m->string (m:m/ (node-box-width node) 2
					)))
	     (format stream "\\draw [~A,~A] (~A.north west) -- ++ (0,-~A);~%" *event-box-line-thickness* *event-box-line-color* (node-name node) (m:m->string (node-box-height node)))))
      ;; event w/o end datetime
      (format stream "\\draw (~A.north west) -- ++(~A,0);"
	      (node-name node) 
	      (m:m->string (node-box-width node))))
  ;; clipping doesn't work with [some? many?] other options... (see section 15.8 of manual for version 2.10)
  (format stream "\\begin{scope}
\\clip (~A.north west) rectangle ++(~A,-~A);
\\node [anchor=north west, color=black, font=\\~A, opacity=0.65] at (~A.north west) {~A};
\\end{scope}
"
	  (node-name node) 
	  (m:m->string (node-box-width node))
	  (m:m->string (node-box-height node))
	  font-size
	  (node-name node)
	  (with-output-to-string (s)
	    (dltx:lit-chars (vevent-summary vevent) s))))

(defun render-event-boxes-NEW (vevents nodes stream)
  (dltx:comment "Write event boxes" :stream stream)
  (map nil #'(lambda (vevent node)
	       (render-event-box vevent node stream))
       vevents nodes))

;; intended to be useful as reference point for generating boxes, labels, etc. 
(defun render-event-node-NEW (vevent-node &key stream)
  "Render a TikZ node corresponding to node VEVENT-NODE."
  (declare (special %hour-height% %line-width% %node-inner-sep%)
	   (stream stream))
  (format stream "\\node [anchor=north west,color=red,draw,fill=white,minimum width=~A,opacity=0,shape=rectangle,line width=1pt] (~A) at (~A,~A) { };
"
	  (m:m->string (node-box-width vevent-node))
	  (node-name vevent-node)
	  ;; at (x,y)
	  (m:m->string (node-x vevent-node))
	  (m:m->string (node-y vevent-node)))
  ;; development: point at nodes
  ;; \draw[->,very thick] (n1) -- (n2);
  ;;(format stream "\\draw[<-,very thick] (~A.north west) -- ++(-1cm,1cm);~%" (node-name vevent-node))
  )

(defun render-event-nodes (vevent-nodes stream)
  "Render a set of nodes useful as reference points."
  (declare (stream stream)) 
  (dltx:comment "Write event nodes" :stream stream)
  (dolist (vevent-node vevent-nodes)
      (render-event-node-NEW
       vevent-node 
       :stream stream)))

(defun set-event-node-x-dimension-values (ordered-vevents vevent-nodes overlap-indices width)
  "Set values associated with x dimension. WIDTH is a measure."
  ;; EV-DONE: list of indices associated with entries which have already been processed
  (let ((number-of-vevents (length ordered-vevents)))
   (let ((ev-done nil))
     ;; iteration over entries
     ;; EV-INDEX - formerly ENTRIES-INDEX
     (dotimes (ev-index number-of-vevents) 
       (let ((overlap-indices-set 
	      ;; consider the event itself to be part of the set 
	      (cons ev-index (nth ev-index overlap-indices)))) 
	 (let ((count 0)
	       ;; UNRENDERED-EV-INDICES
	       (unrendered-ev-indices (set-difference overlap-indices-set ev-done))) 
	   (when unrendered-ev-indices
	     ;; EV-INDEX - formerly ENTRY-INDEX
	     (dolist (ev-index unrendered-ev-indices)
	       (push ev-index ev-done)
	       ;; EV-WIDTH - some fraction of one (where one corresponds to the entire width)
	       (let ((ev-width (/ 1 (length overlap-indices-set))))
		 (incf count)	  ; first entry corresponds to count=1
		 (let ((width-numb (m:m-numb width))
		       (width-unit (m:m-unit width)))
		  (let ((box-width (* width-numb ev-width))
			(x (* ev-width (1- count) width-numb)))
		    (setf (node-x (nth ev-index vevent-nodes))
			  (m:mm x width-unit))
		    (setf (node-box-width (nth ev-index vevent-nodes))
			  (m:mm box-width width-unit)))))))))))))

(defun render-event-start-time-boxes-NEW (vevents nodes params stream)
  (map nil #'(lambda (vevent node)
	       (event-start-time-box node params stream vevent))
       vevents nodes))
