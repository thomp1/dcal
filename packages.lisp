(in-package :cl-user)

(defpackage :dcal
  (:use :cl)
  (:documentation "This program is released under the terms of the Lisp Lesser GNU Public License http://opensource.franz.com/preamble.html, also known as the LLGPL. Copyright: David A. Thompson, 2011-2014")
  (:export dcal dcal2 wcal))