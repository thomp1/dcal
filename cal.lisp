(in-package :dcal)
;;;
;;; cal and high-level functions which are front-ends to cal
;;;
(defun dcal2 (&key events-source (if-exists :supersede) out-path day (delta-hours 36) month todop year)
  "Generate markup for a print-friendly day-level calendar at OUT-PATH. DAY is an integer (representing the day of the month). MONTH is an integer (defaults to the current month). YEAR is an integer (defaults to the current year). Together, DAY, MONTH, and YEAR specify the start time for the calendar. DELTA-HOURS, an integer, specifies the number of hours past start to evaluate and represent. EVENTS-SOURCE is either a list of vevent structures, a string representing icalendar/vcalendar data, or a pathname representing a file containing icalendar/vcalendar data. STYLE is a specifier of the type of calendar to be rendered; the only possible value is :VLIST."
  (cal :events-source events-source
       :if-exists if-exists
       :out-path out-path
       :day day
       :day-layout-style :vlist
       :delta-hours delta-hours
       :event-layout-style :vlist
       :month month
       :todop todop
       :year year))

(defun wcal (&key events-source (if-exists :supersede) out-path day (delta-hours 168) month todop year)
  "Generate markup for a print-friendly week-level calendar at OUT-PATH. The start time for the calendar is specified by DAY (an integer representing the day of the month), MONTH (an integer; defaults to the current month), and YEAR (an integer; defaults to the current year). DELTA-HOURS, an integer, specifies the number of hours past start to evaluate and represent. EVENTS-SOURCE is either a list of vevent structures, a string representing icalendar/vcalendar data, or a pathname representing a file containing icalendar/vcalendar data."
  (cal :events-source events-source
       :if-exists if-exists
       :out-path out-path
       :day day
       :day-layout-style :hlist
       :delta-hours delta-hours
       :event-layout-style :vlist
       :month month
       :todop todop
       :year year))

(defun cal (&key events-source (if-exists :supersede) out-path day (day-layout-style :hlist) (delta-hours 36) (event-layout-style :vlist) month todop year)
  (declare (keyword day-layout-style event-layout-style))
  (multiple-value-bind (curr-s curr-min curr-h curr-day curr-month curr-yr)
      (decode-universal-time (get-universal-time))
    (declare (ignore curr-s curr-min))
    (let ((day (or day curr-day))
	  (month (or month curr-month))
	  (year (or year curr-yr)))
      ;; START/END: universal times at which entire calendar should begin/end
      (let ((start (encode-universal-time 0 0 curr-h day month year)))
	(let ((end (+ start (hours-to-seconds delta-hours)))
	      (vevents (source-to-vevents events-source)))
	  (declare (universal-time end))
	  (let ((file-stream (open out-path
				   :direction :output
				   :if-exists if-exists
				   :if-does-not-exist :create)))
	    ;;
	    ;; page, body, and calendar dimensions and orientation
	    ;;

	    ;; body: total print space (total page size reduced by margin sizes); corresponds to LaTeX 'text area'
	    ;; calendar: print representation of day/hour grid (excludes text notes, TODOs, etc.) and print representation of events associated w/date (even if not linked to a specific time)
	    (let* ((%page-orientation% 
		    (cond ((eq day-layout-style :vlist)
			   "portrait")
			  ((eq day-layout-style :hlist)
			   "landscape")))
		   (body-width (m:mm 25.40 :cm)) 	; ; 7 in = 17.7799 cm
		   (body-height (m:mm 18 :cm))	; 10 in = 25.4 cm
		   (%calendar-width%
		    (if todop
			(m:m- body-width
			    ;; reserve some space for text notes/todo in a 'margin'
			    (m:mm 10.160 :cm)) ; ; 4in = 10.160cm
			body-width))
		   (%calendar-height% body-height)
		   ;; vertical distance above first time point in day
		   (%day-above-distance% 
		    (cond ((eq day-layout-style :vlist)
			   (m:mm 1 :cm))
			  ((eq day-layout-style :hlist)
			   (m:mm 2 :cm))))
		   ;; define vertical distance below time-associated events
		   (%day-below-distance% 
		    (cond ((eq day-layout-style :vlist)
			   (m:mm 1 :cm))
			  ((eq day-layout-style :hlist)
			   (m:mm 3 :cm)))))
	      (declare (special %calendar-height% %calendar-width%
				%day-above-distance% %day-below-distance%
				%page-orientation%)) 
	     (render-calendar2 vevents day-layout-style event-layout-style start end file-stream))
	    (when todop
	      (render-todos "Day0TopNode.north" file-stream))
	    (close file-stream)))))))
