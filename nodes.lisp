(in-package :dcal)
;;
;; nodes (dcal nodes do not exactly parallel tikz nodes)
;;
(defstruct node 
  align
  anchor
  box-height				; a measure
  box-width				; a measure
  fill
  line-width
  minimum-height
  name
  opacity
  x
  y)

;; a node overlaps with another node if either
;;   1. one has a Y value and the other has a Y value and the two are identical
;;   2. one has a DTSTART with a DATE-TIME value and the other has both a DTSTART and a DTEND with a DATE-TIME value and the start time of the former vevent lies within the time range associated with the latter event
(defun nodes-have-y-overlap-p (node1 node2 &optional (y-direction 1))
  "Y-DIRECTION is an integer; given a y, does height extend in a positive or negative direction relative to that y? if Y-DIRECTION is 1, then the box height is added to the y value; if Y-DIRECTION is -1, then we assume a negative direction and the box height is subtracted from the y value."
  (let ((node1-y (m:m-numb (node-y node1)))
	(node2-y (m:m-numb (node-y node2)))
	(node1-box-height (* y-direction (m:m-numb (node-box-height node1))))
	(node2-box-height (* y-direction (m:m-numb (node-box-height node2)))))
    (let ((node1-y-end (+ node1-y node1-box-height))
	  (node2-y-end (+ node2-y node2-box-height)))
     (or 
      ;; if y for 2 lies within y range for 1 or vice-versa
      (and node1-y node2-y node1-y-end 
	   (between node2-y node1-y node1-y-end))
      (and node2-y node1-y node2-y-end
	   (between node1-y node2-y node2-y-end))
      ;; or two start times are identical
      (and node1-y node2-y
	   (= node1-y node2-y))))))
