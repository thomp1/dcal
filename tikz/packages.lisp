(in-package :cl-user)

(defpackage :tkz
  (:use :cl)
  (:documentation "This program is released under the terms of the Lisp Lesser GNU Public License http://opensource.franz.com/preamble.html, also known as the LLGPL. Copyright: David A. Thompson, 2014-2015")
  (:export node tikzpicture top))
