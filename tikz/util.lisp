(in-package :tkz)

;;
;; DCU/shared-string.lisp
(defun list-to-stream (some-list stream &optional (spacer " "))
  "Send content to stream STREAM using the list SOME-LIST where objects in the returned string are separated by string SPACER. Objects in SOME-LIST are rendered with FORMAT ~A

note(s):
  - order retained
  - SPACER does not succeed last item in list
  - clarify differences: at least two other functions in SHARED-STRING have similar functionality:
** this doesn't append spacer to end...
"
  (if (consp some-list)
      (let ((lastitem (car (last some-list)))
	    (len (length some-list)))
	;; everything except last item...
	(dolist (x (subseq some-list 0 (- len 1))) 
	  (if (stringp x)
	      (write-string x stream) 
	      (format stream "~A" x))
	  (write-string spacer stream))
	(if (stringp lastitem)
	    (write-string lastitem stream)
	    (format stream "~A" lastitem)))))


