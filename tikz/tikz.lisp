(in-package :tkz)

;; %%% define a 2-sided node (top,left)
;; \tikzset{rectBL/.style={
;;         draw=none,
;;         append after command={
;;             (\tikzlastnode.north west)
;;         edge [black!50!white] (\tikzlastnode.south west) 
;;             (\tikzlastnode.south west)
;;         edge [black!50!white] (\tikzlastnode.south east) 
;;         }
;;     }
;; }

;; %%% define a 1-sided node (bottom,left)
;; \tikzset{rectL/.style={
;;         %draw=none,
;;         append after command={
;;             (\tikzlastnode.north west)
;;         edge [black!50!white,dashed] (\tikzlastnode.south west) 
;;         }
;;     }
;; }


(defun top (stream &key tikzlibs tikzpagenodesp)
  "TIKZLIBS is a list of strings."
  (declare (list tikzlibs))
  ;; give tikz some space
  (write-string 
   "\\evensidemargin=0in
\\oddsidemargin=0in

\\usepackage{tikz}
" stream)
  (when tikzpagenodesp
    (dltx:usepackage "tikzpagenodes" :stream stream))
  (dolist (tikzlib tikzlibs)
    (dltx:l "usetikzlibrary" tikzlib 
	    :s stream
	    :suppress-newline-p nil)))

;; note: REL-Y deprecated -> use RELXY with either "+" or "++"
(defun node (x y &key rel-y relxy align anchor fill font minimum-height name opacity shape text text-color stream)
  "X is a string representing the x coordinate. Y is a string representing the y coordinate. FILL should be a string representing a color. RELXY, if not NIL, is the string which will precede the coordinate specification (typically '+' or '++')."
  (write-string "\\node " stream)
  ;; options
  (write-char #\[ stream) 
  (let ((option-strings nil))
    (map nil #'(lambda (option option-label)
		 (when option
		   (push (format nil "~A=~A" option-label option) option-strings)))
	 (list align anchor fill font minimum-height opacity shape text-color)
	 '("align" "anchor" "fill" "font" "minimum height" "opacity" "shape" "text"))
    (list-to-stream option-strings stream ", ")) 
  (write-char #\] stream)
  ;; name
  (if name (format stream " (~A) " name))
  ;; position
  (write-string " at " stream)
  (when rel-y 
    (error "Use REL with NODE")
    ;(write-char #\+ stream)
    )
  (if relxy (write-string relxy stream))
  (format stream "(~A,~A) " x y)
  ;; a tikz node must have a text label (even if it is empty)
  (if text
      (dltx:with-enclosing "{" "}" stream (dltx:lit-chars text stream))
      (write-string "{}" stream))
  (write-string ";%
" stream))

(defun node-with-draw (x y &key rel-y align anchor fill minimum-height shape text stream)
  (write-string "\\draw " stream)
  (if rel-y (write-char #\+ stream))
  (format stream "(~A,~A) " x y)
  (write-string "node[" stream)
  ;; options
  (let ((option-strings nil))
    (map nil #'(lambda (option option-label)
		 (when option
		   (push (format nil "~A=~A" option-label option) option-strings)))
	 (list align anchor fill minimum-height shape)
	 '("align" "anchor" "fill" "minimum height" "shape"))
    (list-to-stream option-strings stream ", "))
  (write-char #\] stream)
  (if text 
      (dltx:with-enclosing "{" "}" stream 
	(dltx:lit-chars text stream))
      (write-string "{}" stream))
  (write-string ";%
" stream))

(defun begin-scope (stream &key opts)
  (dltx:l "begin" "scope" :optional-arg opts :opts-position :after :s stream :suppress-newline-p nil))

(defun begin-tikzpicture (stream &key opts)
  (dltx:l "begin" "tikzpicture" :optional-arg opts :opts-position :after :s stream :suppress-newline-p nil))

;; test: (with-output-to-string (s) (tkz:tikzpicture #'(lambda (s1) (write-string "HEY" s1)) s))
(defun tikzpicture (f stream &key opts) 
  (begin-tikzpicture stream :opts opts)
  (if f (funcall f stream))
  (dltx:l "end" "tikzpicture" :s stream))
