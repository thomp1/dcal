(in-package :dcal)
;;
;; dcal-week.lisp: a print-friendly week calendar
;;


;; FIXME: note that this is just a souped-up version of RENDER-CALENDAR

;; CALENDAR-START and CALENDAR-END: date/time (universal time) at which calendar should begin/end
;; EVENT-LAYOUT-STYLE -- :VLIST indicates a vertical list of events (note: this corresponds to former STYLE argument)
;; DAY-LAYOUT-STYLE -- :HLIST indicates that days should be arranged 'side-by-side'; :VLIST indicates that days should be arranged vertically, one under the other
;; WIDTH: a mm/measurement representing the entire width of the area for the calendar component of the document (not including the width of 'todo list' or other components of the document)
(defgeneric render-calendar2 (vevents day-layout-style event-layout-style calendar-start-time calendar-end-time stream))

;; render a vertical list of events:
;;  (day-layout-style (eql :hlist))
;;  (event-layout-style (eql :vlist))
(defmethod render-calendar2 (vevents day-layout-style event-layout-style calendar-start-time calendar-end-time stream) 
  (declare (optimize (safety 3))
	   (special %calendar-height% %calendar-width%
		    %day-above-distance%
		    %day-below-distance%
		    %page-orientation%)
	   (universal-time calendar-start-time calendar-end-time))
  (let ((%day-layout-style% day-layout-style)
	(%line-width% (m:mm 0.4 :pt)) ; default tikz line width is 'thin' -> 0.4pt
	(calendar-start-time	 
	 (cond ((eq day-layout-style :vlist)
		;; if days succeed each other vertically (:vlist) then desirable to have all days cover same range of hours; however, if days succeed each other horizontally, then this it's nice to have things start with CALENDAR-START-TIME
		;; however...
		;; note: if the # of hrs in the first day is allowed to be different than the # of hours spanned by other days, all code which uses a fixed day-length-in-hours--ish value will be thrown off -->> stick with a fixed day length for the time being... only change this once ready to tweak all other code which makes calculations based on # of hours in day(s)
		

		(start-of-day calendar-start-time);calendar-start-time
		)
	       ((eq day-layout-style :hlist)
		(start-of-day calendar-start-time)))))
    (declare (special %day-layout-style%
		      ;%body-width%
		      %line-width%))
    (dltx:documentclass "article" :opts-string %page-orientation% :stream stream)
    (dltx:usepackage "geometry" :opts-string (format nil "~A,letterpaper,pass" %page-orientation%) :stream stream)
    ;; 'backgrounds' is required for 'show background rectangle'
    (tkz:top stream :tikzlibs '("backgrounds" "calendar"))


        (format stream "%%% define a 1-sided node (left)
\\tikzset{rectL/.style={
        %draw=none,
        append after command={
            (\\tikzlastnode.north west)
        edge [~A,~A] (\\tikzlastnode.south west) 
        }
    }
}
"
	    *event-box-line-color*
	    *event-box-line-thickness*)

    
    ;; define a range of hours which merit detailed representation in the calendar
    (let* ((day-range-start-hr 7)
	   (day-range-end-hr 18)
	   (day-range-hours (- day-range-end-hr day-range-start-hr)))
      ;; %WIDTH%: width of area where rendering is occurring
      ;; START-HR and END-HR: integers between 0 and 23 -- actual points at which calendar picks up and leaves off (we adopt an hour-centric view of things...) 
      (let ((%node-inner-sep% (m:mm 0.1 :cm)) 

	    ;; FIXME: phase out use of %width% and replace with %calendar-width%
	    (%width% %calendar-width%))
	(declare (special %node-inner-sep% %node-inner-sep-units% %width%)) 
	;; could specify step sizes: \\begin{tikzpicture}[y=~A,x=\\daywidth] where step size is hour height

	;; make sure text starts below where a typical clipboard clips
	(dltx:l "newgeometry" "top=1in, bottom=0.5in, left=0.5in, right=0.5in" :s stream)
	(dltx:comment "concise month calendar" :stream stream)
	(dltx:document 
	 #'(lambda (document-stream) 
	     (format document-stream "\\begin{tikzpicture}[remember picture, overlay]
")
	     (multiple-value-bind (s min h date mon y day daylight-p z)
		 (decode-universal-time calendar-start-time) 
	       (declare (ignore s min h day daylight-p z))
	       (multiple-value-bind (prev-s prev-min prev-h prev-date prev-mon prev-y prev-day prev-daylight-p prev-z)
		   (decode-universal-time (a-week-ago calendar-start-time)) 
		 (declare (ignore prev-s prev-min prev-h prev-day prev-daylight-p prev-z))
		 (multiple-value-bind (today-s today-min today-h today-date today-mon today-y today-day today-daylight-p today-z)
		     (get-decoded-time)
		   (declare (ignore today-s today-min today-h today-day today-daylight-p today-z))
		   (format document-stream "\\begin{scope}[shift={(current page.north east)}]
\\node [anchor=north east] { \\tikz \\calendar (monthcal) [gray, day xshift=3.5ex, week list, month label above centered, dates=~A-~A-~A to ~A-~A-~A, font=\\small, anchor=center]; };
"
			   (if (> date 7) y prev-y)
			   (if (> date 7) mon prev-mon)
			   ;; with 'week list', if we want to see this week, need to ensure it has 7 d in it within the month of interest 
			   (if (> date 7)
			       (- date 7)
			       ;; because of a TikZ calendar quirk, if this is in the middle of a week, only the next week is shown -->> back up 7 d regardless of whether we're near the start of the month
			       prev-date)
			   ;; move to next month if more than 1/2-way through current month
			   y
			   (if (> date 15) (1+ mon) mon)
			   (if (> date 15) 
			       15
			       (universal-time-date (- (start-of-next-month calendar-start-time) 60))))
		   (format document-stream "\\draw (monthcal-~4,'0',,D-~2,'0',,D-~2,'0',,D) circle (1.5ex);
\\end{scope}" 	     
			   ;; current date
			   today-y
			   today-mon
			   today-date))))
	     (format document-stream "\\end{tikzpicture}~%")
	     ;; use a separate picture for each day
	     (let* ((day-index 0) 
		    (first-day-length-in-hours 
		     (cond ((eq day-layout-style :vlist)
			    (- day-range-end-hr
			       (universal-time-hr calendar-start-time)))
			   ((eq day-layout-style :hlist)
			    (- day-range-end-hr
			       (universal-time-hr calendar-start-time))))))
	       ;; past 'end of day' for first day, bump things to next day
	       (when (< first-day-length-in-hours 0)
		 (setf first-day-length-in-hours day-range-hours)
		 (setf calendar-start-time (start-of-nth-day calendar-start-time 1)))

	       ;; ***** need calendar-start-time to define %hour-height% when day-layout-style is vlist -> wait until here to do that 

	       ;; N-DAYS: integer, total # of days spanned by the calendar
	       (let* ((n-days (days-spanned-by calendar-start-time 
					       ;; FIXME: calendar-end-time hasn't been vetted in the same manner as calendar-start-time -- it might be at 2am on a succeeding day and, in such a case, shouldn't be included in the calculation
					       calendar-end-time))
		      ;; calculate %hour-height% dynamically 
		      (%hour-height% 
		       (cond ((eq day-layout-style :hlist)
			      (m:m/ (m:m- %calendar-height% %day-below-distance% %day-above-distance%)
				    day-range-hours))
			     ((eq day-layout-style :vlist)
			      (m:m/ (m:m- %calendar-height% 
					  (m:m* %day-below-distance% n-days) 
					  (m:m* %day-above-distance% n-days))
				    (* n-days day-range-hours))))))
		 (declare (special %hour-height%)) 
		 ;; render a separate tikz picture for each day
		 (do ((day-start calendar-start-time (start-of-nth-day calendar-start-time day-index)))
		     ((> (+ (* day-index *day-in-seconds*) calendar-start-time) calendar-end-time)) 
		   (let ((shift
			  ;; place this tikzpicture relative to node defined in previous tikzpicture 
			  (cond ((eq day-layout-style :vlist)
				 (if (= 0 day-index)
				     ""
				     (format nil ",shift={(Day~ABottomNode.south)}" (1- day-index))))
				((eq day-layout-style :hlist)
				 (if (= 0 day-index)
				     ""
				     (format nil ",shift={(Day~ATopRightNode.center)}" (1- day-index))))))
			 (day-width
			  (cond ((eq day-layout-style :vlist)
				 %calendar-width%)
				((eq day-layout-style :hlist)
				 (m:m/ %calendar-width% 
				       ;; number of days calendar spans
				       n-days)))))
		     (render-calendar-day day-index day-range-end-hr day-range-start-hr day-start day-width shift document-stream vevents))
		   (incf day-index))))) 
	 :stream stream)))))

(defun render-todos (coordinate stream)
  ;; 'TODO' stuff
  ;; - since these rely on the first hour node, they must succeed the node definition...
  (hline-tikzpicture stream 0.4 
		     :coordinate coordinate
		     :text "TODO (today)")
  (hline-tikzpicture stream 0.8 
		     :coordinate coordinate 
		     :text "if time permits..."))
