# dcal

*Generate a daily calendar/schedule*

---

*dcal* is a Common Lisp tool to generate a print-friendly calendar for a daily or weekly schedule.

## Preparing to use DCAL

`dcal` relies on

- PGF/TiKz
- several Common Lisp systems: [cl-icalendar](https://github.com/davazp/cl-icalendar‎), osicat, unit-formulas, and unix-options.


To use DCAL,

1. Download dcal.
2. Ensure the `dcal.asd` file is accessible to the system manager for the Common Lisp implementation in use.
3. Load the dcal system.


## Defining events via \*EVS\*

Each member of `*EVS*` should have one of two forms.

The first, beginning with a string, describes an event and the associated day and time. Example:

    ("FPU email triage" :mon 9 11)

The second, beginning with a keyword indicating the day of the week, specifies a set of events for a day. Example:

	 (:mon
	  ("S Smith - TUG PD Zoom"      8       8.9 )
	  ("S Smith - TUG PD Zoom"     11.5    12.4 )
	  ("come up for air"           12.5    12.9 )
	  ("email triage"               9      11   )
	  ("TUG PD - R Johnson prep"   13      13.9 )
	  ("snail mail"                14      (20 :min))
	  ("'special times' buffer"    14.5    (30 :min))
	  ("develop sch. proposal"     15      (30 :min)))


## Generating a weekly calendar using \*EVS\*

At the REPL:

```lisp
(setf dcal::*events* (dcal::evs-to-events dcal::*evs*))

;; Generate the calendar as a TeX file:
(dcal:dcal "/path/to/dcal-output.tex")
```

At the shell:

```shell
> pdflatex /path/to/dcal-output.tex
```

Note that multiple invocations of the TeX typesetter (e.g., `pdflatex` or `xelatex`) may be required to generate the desired PDF.



### \*EVENTS\* can be defined directly as well:

```lisp
(setf *events*
      (list
       (make-event :days '(:mon :wed :fri)
		   :hr-start 9
		   :length 50
		   :location "TBD"
		   :label "Lecture")
       (make-event :days '(:mon :wed :fri)
		   :hr-start 8
		   :length 60
		   :fill "black!70!white")
       (make-event :days '(:tue)
		   :hr-start 8
		   :length 480
		   :label "special projects"
		   :fill "black!70!white")))
```


## Generating a day- or day-and-a-half calendar

1. Generate a file with vcalendar data at `/path/to/my-ical-file`
2. Generate the LaTeX file: 
   `(dcal::dcal2 :events-source (make-pathname :directory '(:absolute "path" "to) :name "my-ical-file") :out-path "/root/dcal-day.tex" :style :vlist)`
3. Generate the corresponding PDF file:
   `pdflatex /path/to/dcal-day.tex`


## Generate a weekly calendar

1. Generate a file with vcalendar data at `/path/to/my-ical-file`
2. Generate the LaTeX file:
   `(dcal::wcal :events-source (parse-namestring "/root/.dcal/events.ics") :out-path "/root/.dcal/calendar.tex")`
3. Generate the corresponding PDF file:
   `pdflatex /path/to/dcal-day.tex`


## Miscellany

This project is not associated with the dcal project at sourceforge.net/projects/dcal/ nor with the dcal project at https://github.com/quickfur/dcal.
