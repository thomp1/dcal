(in-package :dcal)
;;;
;;; dcal.lisp - generate a weekly schedule using tikz/pgf
;;;
(defun dcal (pathname &key events (if-exists :supersede) (nubsp t) tags events-as-text-p)
  "Generate a grid representation of a schedule and output it, as
LaTeX, to PATHNAME. If NUBSP is true, place a mark at each hour
position. If TAGS is not NIL, it should be a list of keywords limiting
events shown to those in the specified categories. In this case, any
event with a tag matching any member of TAGS will be shown. If TAGS is
NIL, all events are considered."
  (let ((stream (open pathname	
		      :direction :output
		      :if-exists if-exists
		      :if-does-not-exist :create))
	(landscapep t)
	(paginationp nil))
    ;; article document class
    (dltx:documentclass "article" :opts-string (if landscapep "landscape") :stream stream)
    (unless paginationp
      (dltx:l "pagestyle" "empty"))
    ;; geometry package
    (let ((showframep nil))
      (let ((opts '("letterpaper")))
	(if showframep
	    (push "showframe" opts))
	(if landscapep
	    (push "landscape" opts))
	(dltx:usepackage "geometry" 
			 :opts opts 
			 :stream stream)))
    (tkz:top stream :tikzpagenodesp t)
    (write-string "\\definecolor{vEventBarColor}{RGB}{140,36,36} % vertical bar color for an event
" stream)
    (format stream "%%% define a 1-sided node (left)
\\tikzset{rectL/.style={
        %draw=none,
        append after command={
            (\\tikzlastnode.north west)
        edge [~A,~A] (\\tikzlastnode.south west) 
        }
    }
}
"
	    *event-box-line-color*
	    *event-box-line-thickness*)
    (dltx:document
     #'(lambda (s)
	 (write-string "\\thispagestyle{empty}" s) ; suppress pagination
	 (setup s *day-width* *hour-height*)
	 (picture-top s)
	 (times-markers s 0 :textp t :nubsp t :horiz-lines-p t)
	 (days s :latex :nubsp nubsp :tags tags)
         (write-string *picture-bottom* s)
         (when events-as-text-p
           (write-string "\\clearpage
" s)
           (days-text s :nubsp nubsp :tags tags)))
     :stream stream)
    (close stream)))

;;
;; internal code
;;
(defun day (day stream &key actual-page-bottom-y font-size nubsp tags)
  "DAY is an integer corresponding to the day of the week. Note that the count isn't lisp-ish, beginning at 1 corresponding to MONDAY."
  (let ((day-corner-x (1- day))	; corner of day 1 is at x=0
	)
    (day-vertical-divider day-corner-x actual-page-bottom-y stream)
    (if nubsp
	(times-markers stream day-corner-x :nubsp t))
    (let ((day-center-x (+ day-corner-x 0.5)))
     (format stream
	     "
    % Start a day.
    \\node[anchor=north,font=\\~A] at (~A,~A) {~A};
"
	     (or font-size *day-text-font-size*)
	     day-center-x
	     *picture-y-top*
	     (day-name day)))
    (events (nth (1- day) *day-keywords*) stream :tags tags)))

(defun day-name (day-integer)
  (nth (1- day-integer) day-names))

(defgeneric days (stream format
		  &key date nubsp tags))

;; DAY is a day keyword (see *DAY-KEYWORDS*)
(defun events (day stream &key tags)
  (declare (keyword day)) 
  (write-string "
    % Write the entries.
	% Recall that entry arguments are number of hours (integer) and width 
	% Note that the x coordinate is 1 (for Monday) plus an
    % appropriate amount of shifting. The y coordinate is simply the starting
    % time.
" stream) 
  ;; EVENTS - formerly ENTRIES: ordered by start time
  (let ((events (sort-events-by-start-time (events-for-day day :tags tags))))
    ;; OVERLAP-INDICES - each member corresponds to member of ENTRIES at same position (analyze entries for a given day for overlap) 
    (let ((overlap-indices (overlapping-events-on-day events day))
	  ;; EV-DONE - formerly ENTRIES-DONE: list of indices associated with entries which have already been processed
	  (ev-done nil)
	  ;; EV-WIDTHS - formerly ENTRY-WIDTHS
	  (ev-widths (make-list (length events))))
      ;; iteration over entries
      ;; EV-INDEX - formerly ENTRIES-INDEX
      (dotimes (ev-index (length events))
	(let ((overlap-indices-set (nth ev-index overlap-indices)))
	  ;; N-OVERLAPPING-EV - formerly N-OVERLAPPING-ENTRIES
	  (let ((n-overlapping-ev (length overlap-indices-set))
		(count 0)
		;; PREVIOUSLY-RENDERED-EV-INDICES - formerly PREVIOUSLY-RENDERED-ENTRY-INDICES
		(previously-rendered-ev-indices (intersection overlap-indices-set ev-done))
		;; UNRENDERED-EV-INDICES - formerly UNRENDERED-ENTRY-INDICES
		(unrendered-ev-indices (set-difference overlap-indices-set ev-done))) 
	    (when unrendered-ev-indices
	      ;; EV-INDEX - formerly ENTRY-INDEX
	      (dolist (ev-index unrendered-ev-indices)
		(push ev-index ev-done)
		;; EV - formerly ENTRY
		(let* ((ev (nth ev-index events))
		       ;; which previously rendered events in PREVIOUSLY-RENDERED-EV-INDICES overlap with current event? (some may not overlap...)
		       (indices-overlapping-with-current
			(let ((accum nil))
			  (map nil #'(lambda (prev-rendered-ev-index)
				       (if (ev-overlapp (nth prev-rendered-ev-index events) ev)
					   (push prev-rendered-ev-index accum)))
			       previously-rendered-ev-indices)
			  accum))
		       (width-already-consumed 
			(if indices-overlapping-with-current 
			    (apply #'+ (mapcar #'(lambda (i) (nth i ev-widths)) 
					       indices-overlapping-with-current))
			    0))
		       ;; EV-WIDTH - formerly ENTRY-WIDTH
		       (ev-width (/ (- 1 width-already-consumed) (length unrendered-ev-indices))))
		  (setf (nth ev-index ev-widths) ev-width)
		  (incf count)	  ; first entry corresponds to count=1
		  (let ((start-time (event-hr-start ev))
			(end-time (event-hr-end ev))
			(title (event-label ev))
			(fill (or (event-fill ev)
				  *event-fill*))
			(loc (event-location ev))
			(day-integer (1+ (position day *day-keywords*))))
		    ;; PGF \node currently doesn't allow a maximum height to be explicitly specified -> thus, box may end up larger than the actual time slot
		    ;;(pgf-node-PURE :count count :day-integer day-integer :end-time end-time :ev-width ev-width :fill fill :loc loc :n-overlapping-ev n-overlapping-ev :start-time start-time :stream stream :title title)
		    ;; kludge -> ensure box is for correct time slot even if text doesn't fit
		    (pgf-node-KLUDGE :count count :day-integer day-integer :end-time end-time :ev-width ev-width :fill fill 
				     :font-size *event-text-font-size*
				     :loc loc :n-overlapping-ev n-overlapping-ev :start-time start-time :stream stream :title title)))))))))))

;; DAY is a day keyword
(defun events-for-day (day &key tags)
  (declare (keyword day))
  (let ((event-set (if tags
		       (events-with-tags *events* tags)
		       *events*)))
   (loop for day-event in event-set
      when (member day (event-days day-event))
      collect day-event)))

(defun events-with-tags (events tags)
  (loop for event in events
      when (member (event-tag event) tags)
      collect event))

(defun pgf-node-KLUDGE (&key count day-integer end-time ev-width fill (font-size "footnotesize") loc n-overlapping-ev start-time stream title)
  ;; COUNT: number corresponding to nth event at given time slot (count isn't lisp-ish and starts at 1)
  ;; DAY-INTEGER: number corresponding to position of day in *DAY-KEYWORDS* (count isn't lisp-ish and starts at 1)
  ;; FONT-SIZE: string specifying LaTeX font size (e.g., "footnotesize")

  ;; draw rectangle first, then put text on top
  (format stream "    \\node[font=\\~A, entry={~A}{~A}{~A}] at (~A,~A) { };
" 
	  font-size
	  (- end-time start-time) ; height of box -- the length (in hrs) of entry
	  ;; width of box:
	  (format nil "\\daywidth*~A-0.6666em-0.4pt" ev-width)
	  fill
	  ;; (x,y)
	  ;; x position of days starts at 0 but days are numbered from 1
	  (if (> n-overlapping-ev 1)
	      (+ (1- day-integer) (/ (1- count) n-overlapping-ev)) 
	      (1- day-integer))
	  start-time)
  ;; calculate x position of text
  ;; - left and right edges of first column are at 0 and 1, etc.
  (let* ((xleft  (+ (1- day-integer) (/ (1- count) n-overlapping-ev)))
	 (xright (+ (1- day-integer) (/ count n-overlapping-ev)))
	 (xcenter (/ (+ xleft xright) 2))
	 (ytop start-time)
	 (ybottom end-time)
	 (ycenter (/ (+ ytop ybottom) 2)))
    
    (format stream "    \\node[font=\\~A, align=center, shape=rectangle, text width=~A] at (~A,~A) { ~A };
" 
	    font-size
	    (format nil "\\daywidth*~A-0.6666em-0.4pt" ev-width)
	    ;;(- end-time start-time) ; don't allow text to exceed this height
	    (float xcenter)		; center x position
	    ;; ! while lisp handles numbers like 19763/1440 with ease, pgf/tikz chokes -> use floats here and elsewhere)
	    (float ycenter)		; center y position 
	    ;; event text
	    (with-output-to-string (s)
	      (when title 
		(write-string title s) 
		(write-string " " s)) 
	      (when loc
		(write-string loc s))))))

(defun pgf-node-PURE (&key count day-integer end-time ev-width fill (font-size "footnotesize") loc n-overlapping-ev start-time stream title)
  (format stream "    \\node[font=\\~A, entry={~A}{~A}{~A}] at (~A,~A) {~A};
"
	  font-size
	  (- end-time start-time) ; height of box -- the length (in hrs) of entry
	  ;; width of box:
	  (format nil "\\daywidth*~A-0.6666em-0.4pt" ev-width)
	  fill
	  ;; (x,y)
	  ;; x position of days starts at 0 but days are numbered from 1
	  (if (> n-overlapping-ev 1) 
	      (+ (1- day-integer) (/ (1- count) n-overlapping-ev)) 
	      (1- day-integer))
	  start-time 
	  ;; event text
	  (with-output-to-string (s)
	    (when title 
	      (write-string title s) 
	      (write-string " " s)) 
	    (when loc
	      (write-string loc s)))))

(defun setup (stream day-width hour-height)
  "DAY-WIDTH is a measure. HOUR-HEIGHT is a measure."
  (format stream "
% These set the width of a day and the height of an hour.
\\newcommand*\\daywidth{~A}
\\newcommand*\\hourheight{~A}

% The entry style will have two options:
% * the first option sets how many hours the entry will be (i.e. its height);
% * the second option sets how many overlapping entries there are (thus
%   determining the width).
\\tikzset{entry/.style n args={3}{
    draw,
    rectangle,
    anchor=north west,
    line width=0.4pt,
    inner sep=0.3333em,
    text width={#2},
    minimum height=#1*\\hourheight,
    align=center,
    fill=#3
}}
"
	  (m:m->string day-width)
	  (m:m->string hour-height)))

;;
;; text representation of events
;;
(defun day-text (day stream &key tags)
  "DAY is an integer corresponding to the day of the week. Note that the count isn't lisp-ish, beginning at 1 corresponding to MONDAY."
  (format stream
          "
    % Start a day.

~A
\\vspace{0.1cm}
\\hrule
\\vspace{0.1cm}
"
          (day-name day))
  (unless (events-text (nth (1- day) *day-keywords*) stream :tags tags)
    (write-string "\\vspace{1cm} " stream)))

(defun days-text (stream &key nubsp tags)
  (let ((start-day (car *day-range*))
        (last-day (cdr *day-range*)))
    (do ((day start-day (1+ day)))
        ((> day last-day))
      (day-text day stream :nubsp nubsp :tags tags))))

(defun event-text (&key end-time loc start-time stream title)
  "COUNT is an integer specifying the nth event at the given time slot
(note that count isn't lisp-ish and starts at 1). DAY-INTEGER: number
corresponding to position of day in *DAY-KEYWORDS*."
  ;; draw rectangle first, then put text on top
  (format stream " ~A - ~A  "
          (h-to-formatted-h start-time) (h-to-formatted-h end-time))
  ;; calculate x position of text
  ;; - left and right edges of first column are at 0 and 1, etc.
  (format stream " ~A \\\\
"
          ;; event text
          (with-output-to-string (s)
            (when title
              (write-string title s)
              (write-string " " s))
            (when loc
              (write-string loc s)))))

(defun events-text (day stream &key tags)
  "DAY is a day keyword (see *DAY-KEYWORDS*). Return the events under
consideration for the specified day."
  (declare (keyword day))
  (write-string "
    % Write the entries.
        % Recall that entry arguments are number of hours (integer) and width
        % Note that the x coordinate is 1 (for Monday) plus an
    % appropriate amount of shifting. The y coordinate is simply the starting
    % time.
" stream)
  ;; EVENTS - formerly ENTRIES: ordered by start time
  (let ((events (sort-events-by-start-time (events-for-day day :tags tags))))
    ;; iteration over entries
    (dolist (ev events)
      (let ((start-time (event-hr-start ev))
            (end-time (event-hr-end ev))
            (title (event-label ev))
            (loc (event-location ev)))
        (event-text :end-time end-time :loc loc :start-time start-time :stream stream :title title)))
    events))

