(in-package :dcal)
;;
;; dcal-week.lisp: a print-friendly week calendar
;;
(defun render-calendar-day (day-index day-range-end-hr day-range-start-hr day-start day-width shift stream vevents &optional (all-other-events-p t))
"FIRST-DAY-LENGTH is the length, in hours, of the time range under consideration for the first day. DAY-START is the universal time associated with the start of the time range for the current day. DAY-WIDTH is a measure representing the width of the box for a single day. VEVENTS is a list of vevent objects. Place this tikzpicture at SHIFT where SHIFT specifies a TikZ coordinate or anchor (e.g., '(mybox.south)' or '(0,2)')."
  (declare (special %calendar-height% %calendar-width%
		    %day-above-distance%
		    %line-width% %node-inner-sep% %node-inner-sep-units% %hour-height% )
	   (number day-index day-range-end-hr day-range-start-hr day-start)) 
  ;; show frame: show background rectangle, inner frame sep=0mm
  ;; -> need \usetikzlibrary{backgrounds}
  (dltx:comment (format nil "begin the calendar for day ~A" day-index) :stream stream)
  ;; FIXME: (tkz:tikzpicture ...)
  (format stream "\\begin{tikzpicture}[ show background rectangle,remember picture,overlay~A ] " shift)
  (let* ((x 0)
	 ;; Y: position of start of current day
	 (y (m:mm 0 (m:m-unit %hour-height%)))
	 (day-top-margin-height %day-above-distance%) ; distance reserved at the top for line with date text
	 (day-main-y (m:m- y day-top-margin-height))
	;; VEVENTs of interest:
	;; - those with a DTSTART DATE-TIME specification with a start value in the range defined by START-END
	;; - those with a DTSTART DATE specification
	 (vevents-for-day (sort-vevents vevents 
					#'(lambda (vevent)
					    (time-in-day-p
					     day-start
					     (vevent-dtstart vevent))))))
    ;; push DAY-START to maximum of day-start or day-range-start-hr
    (setf day-start 
	  (adjust-to-at-least day-start :hr day-range-start-hr))
    (let ((day-end (adjust-to day-start :hr day-range-end-hr)))

      ;; FIXME: mm should handle this deficiency -- shouldn't need to sprinkle this throughout code
      (assert (equalp (m:m-unit %hour-height%) 
		      (m:m-unit %calendar-height%)))	; if this isn't the case, use UNIT-FORMULA or another system to handle unit interconversions

      ;; if we're past the end of the page, cease and desist...
      (unless (> (abs (m:m-numb y)) (abs (m:m-numb %calendar-height%)))
	;; define nodes at the upper left- and right-hand corners of the box representing the Nth day as 'DayNTopNode'
	(tkz:node x
		  (m:m->string y)
		  :fill "red" :opacity 0
		  :align "left" :minimum-height "0.1cm" 
		  :name (format nil "Day~ATopLeftNode" day-index)
		  :shape "circle" :stream stream)
	(tkz:node (m:m->string day-width)
		  (m:m->string y)
		  :fill "black" :opacity 0
		  :align "left" :minimum-height "0.1cm" 
		  :name (format nil "Day~ATopRightNode" day-index)
		  :shape "circle" :stream stream)
	;; date as a 'header'
	;; - positioned in 2cm gap (see DAY-TOP-MARGIN-HEIGHT) which precedes hours
	(let ((date-hd-y (m:m->string (m:mm (- (m:m-numb y) 
					       (- (m:m-numb day-top-margin-height) 0.7))
					    (m:m-unit %hour-height%))))
	      (date-hd-x-start x)
	      (date-hd-x-end (m:m->string day-width))
	      (date-text (format nil "~A ~2,'0',,D-~A" 
				 (universal-time-day-abbr3 day-start)
				 (universal-time-month day-start)
				 (universal-time-date day-start))))
	  ;; start each day with a vertical line with date above line
	  (let ((text-color (if (weekend-day-p day-start)
				"black!70!white,"
				"")))
	    (format stream "\\draw (~A,~A) -- (~A,~A) node [~Aanchor=north west,midway,above, minimum height=0.5cm] {~A};~%" 
		    date-hd-x-start
		    date-hd-y
		    date-hd-x-end
		    date-hd-y
		    text-color
		    date-text)))
	;; establish set of events for the day:
	;; 1. events within day-start and day-end
	;; 2. events without associated times
	;; 3. events with associated times but which aren't in range defined by day-start and day-end
	(multiple-value-bind (vevents-in-range vevents-other)
	    (sort-vevents vevents-for-day
			  #'(lambda (vevent)
			      (vevent-begins-in-range-p vevent day-start day-end)))
	  ;; TIMES-MARKERS2 generates named nodes at each hour of the day
	  (times-markers2 day-index
			  stream 
			  x
			  day-main-y
			  day-start 
			  day-end
			  :textp t :nubsp t :horiz-lines-p t) 
	  (render-events-in-range vevents-in-range 
				  day-main-y
				  day-start
				  day-width
				  stream) 
	  (render-other-events
	   ;; ALL-OTHER-EVENTS-P nil --> only interested in vevents where DTSTART value is associated with a DATE value type (not DATE-TIME)
	   (if all-other-events-p
	       vevents-other
	       (sort-vevents vevents-other 
			     #'(lambda (vevent)
				 (string=
				  (vevent-dtstart-value-type vevent)
				  "DATE"))))
	   (m:mm (- (m:m-numb day-main-y) 
		    (* (m:m-numb %hour-height%) 
		       (length (hours-in-range day-start day-end))))
		 (m:m-unit %hour-height%))
	   stream
	   day-index)
	  (render-bottom-node day-index x y stream)))))
  (write-string *picture-bottom* stream))

(defun nub2 (x y &optional stream fill)
  ;; note that 'down' corresponds to negative y in tikz
  (format stream "\\filldraw[fill=~A] (~A,~A) circle (0.1em);~%" (or fill "black") x y))

(defun render-bottom-node (day-index x y stream)
  ;; generate a final named node...
  (tkz:node x 
	     (m:m->string 
	      (m:m- y (m:mm 1 (m:m-unit y)))) 
	     :align "left" :minimum-height "0.5cm" 
	     :name (format nil "Day~ABottomNode" day-index)
	     :shape "circle" :stream stream))

;; assumes being called in the context of RENDER-CALENDAR2
(defun render-events-in-range (vevents y y-time width stream) 
  "Y is a measure specifying the y position (negative means 'down' (tikz) which corresponds to the upper-most component of rendering for this series of events -- this position maps to time Y-TIME (a universal time). WIDTH is a measure indicating the width of the area in which the calendar is to be rendered. VEVENTS is an unordered set of vevents."
  (declare (special %line-width%))
  ;; VEVENTS-ORDERED: ordered by start time (from highest/latest to lowest/earliest)
  (let ((vevents-ordered (sort-vevents-by-dtstart vevents)))
    ;; (1) populate y values of nodes first
    ;; (2) then determine overlap
    ;; (3) then generate x/width values
    
    ;; VEVENT-NODES: list of node structures corresponding to VEVENTS-ORDERED
    ;; - EVENT-NODE03 populates y slot, box-height slot, and name slot
    (let ((vevent-nodes (mapcar #'(lambda (vevent)
				    (event-node03 vevent y y-time))
				vevents-ordered)))
      (let ((nodes-overlap-indices
	     (pairwise-comparison-indices 
	      vevent-nodes
	      #'(lambda (node1 node2)
		  ;; TikZ y axis extending downwards corresponds to increasingly negative values
		  (nodes-have-y-overlap-p node1 node2 -1)))))
	;; OVERLAP-INDICES: a list of lists
	;; - each sublist corresponds to the member of VEVENTS-ORDERED at that position; the integers in the sublist correspond to the positions of other members of VEVENTS-ORDERED which overlap with the event at that position 
	(let ((overlap-indices nodes-overlap-indices)) 
	  (set-event-node-x-dimension-values vevents-ordered vevent-nodes overlap-indices width)
	  ;; as it is difficult to specify the height of a TikZ node...
	  ;; 1. generate node where only width is defined
	  ;; 2. stick a rectangle on top of it
	  ;; 3. clip content
	  ;; - putting nodes marking locations in place first allows iteration over nodes as needed...
	  (render-event-nodes vevent-nodes stream)
	  (render-event-boxes-NEW vevents-ordered vevent-nodes stream)    
	  ;; EVENT-START-TIME-BOX-PARAMS: can include :above, :below, :centered, or be empty (no start time box rendered) 
	  (let ((event-start-time-box-params '(:center)))
	    (render-event-start-time-boxes-NEW vevents-ordered vevent-nodes event-start-time-box-params stream)))))))

;; Y is a measure representing the y position (tikz) (negative is 'down') -- units are those of %hour-height%
(defun render-other-events (vevents y stream day-index &key (font-size "footnotesize"))
  (declare (ignore day-index) 
	   (special %hour-height%))
  (let ((x 0)
	(event-text-renderer 
	 #'(lambda (vevent)
	     (with-output-to-string (s)
	       (if (vevent-dtstart vevent)
		   (let ((dtstart (vevent-dtstart vevent))) 
		     (format s "~2,'0',,D:~2,'0',,D" (universal-time-hr dtstart) (universal-time-min dtstart)))) 
	       (write-string (vevent-summary vevent) s)))))
    (when vevents
      ;; render first node separately from the remaining nodes
      (tkz:node x (m:m->string y)
		:anchor "north west"
		:align "left"
		:fill "white"
		:font (concatenate "\\" font-size)
		:opacity "1.0"
		:stream stream
		:text (funcall event-text-renderer (pop vevents)))
      (let ((line-height (m:mm 0.5 (m:m-unit %hour-height%))))
	(dolist (vevent vevents) 
	  (setf y (m:m- y line-height))
	  (tkz:node x
		    (m:m->string y)
		    :anchor "north west"
		    :align "left"
		    :fill "white"
		    :font (concatenate "\\" font-size)
		    :opacity "1.0"
		    :stream stream
		    :text (funcall event-text-renderer vevent)))))))

(defun times-markers2 (day-index stream x y start end &key (font-size "footnotesize") textp nubsp horiz-lines-p)
  "Proceeding along the vertical axis, add text representations of time if TEXTP is true and add 'nubs' if NUBSP is true. Horizontal position is specified as X. A vertical axis offset is specified with Y where Y is a measure and where a negative values corresponds to 'down'. DAY-INDEX: an integer; the first day on the calendar is at index 0. START/END: date/time (universal time) at which times-markers2 should start/end."
  (declare (special %calendar-height% %day-layout-style% %hour-height% %width%))
  (dltx:comment "times markers" :stream stream)
  (let ((hours (hours-in-range start end))
	(not-first-day-p (> day-index 0))
	(horizontal-day-layout-p (eq %day-layout-style% :hlist)))
    (dolist (hr hours)
      ;; HR: an integer between 0 and 24     
      (let ((ushr (cond ((> hr 12)
			 (- hr 12))
			((= hr 0)
			 12)
			(t hr)))
	    ;; note that 'down' is negative on the y axis in tikz
	    (this-y ;; (format nil "~A~A" 
		    ;; 	    (+ (m:m-numb y)
		    ;; 	       (* -1
		    ;; 		  (- hr (first hours)) ; # of hrs
		    ;; 		  (m:m-numb %hour-height%))) 
		    ;; 	    (m:m-unit %hour-height%))
	      (m:m->string (m:mm
			    			    (+ (m:m-numb y)
			       (* -1
				  (- hr (first hours)) ; # of hrs
				  (m:m-numb %hour-height%)))
			    (m:m-unit %hour-height%)
			    ))
	      
	      
	      ))
	;; if we're past the end of the page, cease and desist...
	(assert (equalp (m:m-unit %hour-height%) (m:m-unit %calendar-height%))) ; if this isn't the case, use UNIT-FORMULA or another system to handle unit interconversions
	(if (> (abs (m:m-numb y)) 
	       (abs (m:m-numb %calendar-height%)))
	    (return)
	    (progn 
	      ;; put a node here so we can reference it later 
	      ;; - node has name DayNTimeM where N is the day number (relative day; 0 corresponds to the first day in the calendar) and M is number representing hour of day (in range 0 to 24) 
	      (format stream "\\node (Day~AHour~A) at (~A,~A) {~A};~%" day-index hr x this-y 
		      ""		; ushr
		      ) 
	      (if textp
		  ;; if day layout is horizontal (hlist) then...
		  (if (and horizontal-day-layout-p not-first-day-p)
		      (format stream "\\node[anchor=south east,color=black!50!white,font=\\~A] at (~A,~A) {~A};~%" font-size x this-y ushr)
		      (format stream "\\node[anchor=east] at (~A,~A) {~A};~%" x this-y ushr))
		  "")
	      (if nubsp
		  (nub2 x this-y stream 
			(if (and horizontal-day-layout-p not-first-day-p) 
			    "gray"))
		  "")
	      (if horiz-lines-p
		  (format stream "\\draw[color=gray!50!white, thin] (~A,~A) -- (~A,~A);~%"
			  x this-y (m:m->string %width%) this-y)
		  "")))))))
