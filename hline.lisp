(in-package :dcal) 

(defun hline (x-shift y-shift line-length stream &key (coordinate "current page.north west") node-options (text " "))
  "Draw a horizontal line. COORDINATE is a string representing the absolute coordinate; everything else is calculated relative to this coordinate; default to upper left-hand corner of the page."
  ;; draw a line starting at the page origin and extending right; then shift that line to the right and down
  (format stream "
\\begin{scope}[shift={(~A)}]
\\draw [thick,xshift=~A,yshift=~A] (0,0) -- +(~A,0);
\\end{scope}
\\draw (~A) [xshift=~A,yshift=~A] node [rectangle, draw, anchor=north west, minimum height=0.5cm~A] {~A};
"
	  coordinate
	  x-shift
	  y-shift
	  line-length
	  ;; label
	  coordinate
	  x-shift
	  y-shift
	  (if node-options (concatenate 'string "," node-options) "")
	  text))

;; VERTICAL-STAGGER: 0.6 corresponds to 60% down body
(defun hline-tikzpicture (stream vertical-stagger &key coordinate (font-size "footnotesize") (text "SOMETHING"))
  (declare (special %body-height% %body-width% %width%))
  (format stream "
% TODO
\\begin{tikzpicture}[remember picture, overlay]
")
  (hline 	 
   (m:m->string %width%)
   (m:m->string (m:m* %body-height% -1 vertical-stagger))
   (m:m->string (m:m- %body-width% (m:m* %width% 1.1)))
   stream
   :coordinate coordinate
   :node-options (format nil "font=\\~A" font-size)
   :text text)
  (format stream "\\end{tikzpicture}~%"))
