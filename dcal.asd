(defsystem :dcal
  :description "Generate an events calendar using PGF/TikZ"
  :serial t
  :components ((:module "measures"
			:components ((:file "packages") 
				     (:file "units") 
				     (:file "measures"))) 
	       (:file "packages")
	       (:file "util")
	       (:file "events")
	       (:file "hline")
	       ;(:file "measures")
	       (:file "nodes")
	       (:file "vars") 
	       (:file "latex")
	       (:file "dcal")
	       (:file "icalendar")
	       (:file "event-nodes")
	       (:file "week")
	       (:file "day")
	       (:file "cal")
	       (:file "executable")
	       (:file "evs")
	       )
  :depends-on (:cl-icalendar
	       :osicat
	       :tikz
	       :unit-formulas		; measures
	       :unix-options))
