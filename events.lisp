(in-package :dcal)
;;;
;;; events.lisp - event objects
;;;
(defstruct event
"DAYS -  specification of day(s) of week - list of integers; numbering is consistent with LOCAL-TIME -> MON=1; note that this is really a specialized recurrence rule describing a recurrence pattern within a week
DESCRIPTION -  ; longer, detailed description; a string
FILL   - string corresponding to fill color (default is white) 
HR-START - start time (a number ranging from 0 to 23 (military time); NIL if start time unknown)
LENGTH - length of a single event instance in minutes (integer)
LOCATION - a string
MONTH - integer 1-12 (corresponding to Jan through Dec)
RECURRENT - :daily, :weekly, :monthly, :yearly, ... or a closure which calculates dates (as ...?) given a start/end range to work with? 
START  - start time, expressed as a lisp universal time
TAG  - a single keyword describing the primary category this event falls into (e.g., :work, :personal, etc.)
TITLE  - title (string)
YEAR - integer corresponding to AD year (e.g., 2013)"
  days 
  description
  fill
  hr-start
  label
  length
  location
  month
  recurrent
  tag
  year)

;; look for time overlap (ignore day)
(defun ev-overlapp (ev1 ev2)
  (let ((st2 (event-hr-start ev2))
	(et2 (event-hr-end ev2))
	(st1 (event-hr-start ev1))
	(et1 (event-hr-end ev1)))
    ;; ignore overlap at boundaries  - comparing 12-13 to 13-14
    (or (and (>= st2 st1) (< st2 et1))
	(and (> et2 st1) (<= et2 et1)))))

;; return, in same units as hr-start slot, the end time for the event
(defun event-hr-end (ev)
  (+ (event-hr-start ev) (/ (event-length ev) 60)))

(defun events-with-start-times (events)
  "Given list of events EVENTS, return multiple values: (1) a list of events for which a start time is specified and (2) a list of events for which a start time is not specified."
  (loop for ev in events
     if (event-hr-start ev) collect ev into c-time
     else collect ev into c-notime
     finally (return (values c-time c-notime))))

;; check for events which overlap with respect to time (assume events on same day)
;; return a list of lists (e.g., ((1 0) (1 0)) would indicate that ...
(defun overlapping-events-on-day (events day)
  ;; EVENTS is a list of event structures
  ;; DAY is a day keyword
  (declare (ignore day))		; no need to use DAY (yet?)
  (mapcar #'(lambda (ev)
	      (let* (;(est (event-hr-start ev))
		     ;(eet (+ est (event-length ev)))
		     (indices nil))
		;; look for overlap with other entries
		(dotimes (x (length events))
		  (let ((comparison-ev (nth x events))) 
		    ;; ignore overlap at boundaries  - comparing 12-13 to 13-14
		    (if (ev-overlapp ev comparison-ev)
			(push x indices))))
		indices))
	  events))

(defun sort-events-by-start-time (events)
  "Sort events with start times by start time. Return events without start times as a second value."
  (multiple-value-bind (events-times events-notimes)
      (events-with-start-times events)
    (values
     (sort events-times 
	   #'(lambda (ev1 ev2)
	       (< (event-hr-start ev1) (event-hr-start ev2))))
     events-notimes)))
