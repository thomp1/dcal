(in-package :dcal)

;; an example of a list of events
(defvar *events*
  (list
   ;; courses
   (make-event :days '(:mon :wed :fri)
	       :hr-start 12.25
	       :length 50
	       :location "AIH 113"
	       ;:recurrent dum4
	       :label "BIOL 420L Lect"
	       :description "Biochemistry Lecture")
   (make-event :days '(:wed)
	       :hr-start 14.5
	       :length 180
	       :location "AIH 217"
	       :label "BIOL 420L Lab"
	       :description "Biochemistry Lab")
   (make-event :days '(:tue)
	       :hr-start 17.67
	       :length 100
	       :location "AIH 113"
	       :label "BIOL 220 Lect"
	       :description "Healthcare in the US Lecture")
   (make-event :days '(:mon :wed :fri)
	       :hr-start 9
	       :length 50
	       :location "AIH 227"
	       :label "CHEM 311 Lect"
	       :description "Organic Chemistry II Lect")
   ;; course prep
   (make-event :days '(:mon :wed :fri)
	       :hr-start 8
	       :length 60
	       :fill "black!70!white")
   (make-event :days '(:mon :wed)
	       :hr-start 11
	       :length 75
	       :fill "black!70!white")
   (make-event :days '(:tue)
	       :hr-start 16.5
	       :length 60
	       :fill "black!70!white")
   (make-event :days '(:wed)
	       :hr-start (+ 13 (/ 5 60))
	       :length (- 90 (+ 13 (/ 5 60)))
	       :fill "black!70!white")
   (make-event :days '(:fri)
	       :hr-start 10
	       :length 60 
	       :fill "black!70!white")
   ;; office hours
   (make-event :days '(:mon)
	       :hr-start 13.2
	       :length 120
	       :label "Office Hours"
	       :fill "white!70!black")
   (make-event :days '(:tue)
	       :hr-start 14.5
	       :length 90
	       :label "Office Hours"
	       :fill "white!70!black")
   ;; low-productivity time
   (make-event :days '(:mon :tue :wed :thu :fri)
	       :hr-start 14.5
	       :length 120
	       :fill "white!70!black")
   ;; personal
   (make-event :days '(:fri)
	       :hr-start 15.5
	       :length 120
	       :label "C/E walk"
	       :fill "black!70!white")
   (make-event :days '(:fri)
	       :hr-start 11
	       :length 50
	       :label "lunch"
	       :fill "black!70!white")
   ;; research
   (make-event :days '(:thu)
	       :hr-start 8
	       :length 480
	       :label "RES"
	       :fill "black!70!white")))

(defparameter *day-range* '(1 . 5))

;;
;; internal vars
;;

;; original in SRC/DAT-TIME.LISP
(defparameter *day-keywords*
  '(:mon :tue :wed :thu :fri :sat :sun))

(defparameter day-names 
  ;;'("Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday" "Sunday")
  '("M" "T" "W" "R" "F"))

(defparameter *day-text-font-size* "large")
(defparameter *day-width* (m:mm 3.5 :cm))

(defparameter *event-fill* "white!95!black"
  "Default background color for an event.")

(defparameter *event-box-line-color* "black!50!white"
  "TikZ color")

(defparameter *event-box-line-thickness* "thick"
  "TikZ line thickness")

(defparameter *event-start-time-font-size* "scriptsize"
  "A standard LaTeX font size (e.g., tiny, scriptsize, footnotesize, small, ...).")

(defparameter *event-text-font-size* "large")

;; note: hour height of 6 em pushes content off of page in landscape format; 4 em works well but units of em make it problematic to determine where edge of page is in absolute units
(defparameter *hour-height* (m:mm 1.3 :cm))
(defparameter *hour-text-font-size* "large")

(defparameter *picture-bottom*
"\\end{scope}
\\end{tikzpicture}
")

;; FIXME: use TKZ tikzpicture
;; FIXME: this scope kludge is ugly - should calculate values from vars and should use a scope function which automatically provides closing scope (see also *picture-bottom*)
(defparameter *picture-top*
"
% Start the picture and set the x coordinate to correspond to days and the y
% coordinate to correspond to hours (y should point downwards).
% Set 0,0 to correspond to the upper left-hand corner of the page.
\\begin{tikzpicture}[remember picture, overlay, shift={(current page.north west)},y=-\\hourheight,x=\\daywidth]

% upper LH corner is current page.north west
\\draw (current page.north west) circle (1cm);
\\draw (0,0) circle (0.5cm);
\\node[font=large] at ++(4cm,0) {Good morning};

% shift position relative to the upper left-hand corner
% - providing a left margin of 1cm
% - providing a top margin of 1 cm and then offsetting so that internal vertical units of 6.5 begin at the top of the page ((*hour-height* 6.5)-1)
\\begin{scope}[shift={(1cm,7.45cm)}]
	\\filldraw[fill=green] (0,0) circle (1cm);

")

(defun picture-top (stream)
  (write-string "
% Start the picture and set the x coordinate to correspond to days and the y
% coordinate to correspond to hours (y should point downwards).
% Set 0,0 to correspond to the upper left-hand corner of the page.
" stream)
  (tkz::begin-tikzpicture
   stream
   :opts '("remember picture" "overlay" "shift={(current page.north west)}" "y=-\\hourheight" "x=\\daywidth"))
  (write-string
   "

% upper LH corner is current page.north west
%\\draw (current page.north west) circle (1cm);
%\\draw (0,0) circle (0.5cm);
%\\node[font=large] at ++(4cm,0) {Good morning};

"
   stream)
  ;; % shift position relative to the upper left-hand corner
  ;; % - providing a left margin of 1cm
  ;; % - providing a top margin of 1 cm and then offsetting so that internal vertical units of 6.5 begin at the top of the page ((*hour-height* 6.5)-1)
  (tkz::begin-scope
   stream
   :opts (list (format nil "shift={(~A,~A)}"
		       (m:m->string *page-left-margin*)
		       (m:m->string (m:m- (m:m* *hour-height* 6.5) (m:mm 1 :cm)))
		       ))
   )
  (write-string
   "
%	\\filldraw[fill=green] (0,0) circle (1cm);

" stream)
  )

;;
;; page dimensions and parameters
;;
(defparameter *page-left-margin* (m:mm 1 :cm))

;; consider using PAPER-SIZES
(defparameter *physical-page-bottom-y* (m:mm 21.59 :cm))

;; FIXME: no markup should occur above this point
;; present status:
;; [x] vertical lines honor this
;; [x] day labels honor this
;; [ ] events, times, etc. do *not* honor this
(defparameter *picture-y-top*
  6.5
  "The y coordinate (TikZ relative coordinates) of the upper-left-hand corner of the image.")


;;
;; time range for day
;;
(defparameter *day-start-time* 7
  "The calendar will be represented beginning at this time. Events at
  or past this time, but before *day-end-time*, will be shown.")

(defparameter *day-end-time* 17.5
  "The calendar will be represented ending at this time. Events at or
  before this time, but after *day-start-time*, will be shown.")
