(in-package :dcal)
;;;
;;; utilities based on CL-ICALENDAR 
;;;
(defun calendar->vevents (calendar)
  (let ((subcomponents (cl-icalendar::subcomponents calendar)))
    ;; ignore other components (alarms, todos, freebusy, ...) 
    (loop for component in subcomponents
       when (typep component 'cl-icalendar::vevent)
       collect component)))

;; check for events which overlap with respect to time
;; return a list of lists (e.g., ((1) (0)) would indicate that, e.g., for (1), event at position 0 has overlapping event at position 1
(defun overlapping-vevents (vevents)
  (pairwise-comparison-indices vevents 'vevents-overlap-p))

(defun sort-vevents (vevents predicate)
  "Return multiple values: a list of vevents which, when passed as arguments to the funcallable object PREDICATE, yield a true value and, as a second value, the remaining vevents (those which don't yield a true value)."
  (multiple-value-bind (matches others) 
      (loop for vevent in vevents
	 if (funcall predicate vevent)
	 collect vevent into matches
	 else collect vevent into others
	 finally (return (values matches others)))
    (values (alexandria:flatten matches)
	    (alexandria:flatten others))))

(defun sort-vevents-by-dtstart (vevents)
  (sort vevents #'(lambda (vevent1 vevent2) 
		    (> (vevent-dtstart vevent1)
		       (vevent-dtstart vevent2)))))

(defgeneric source-to-vevents (events-source)
  (:documentation "Return a list of CL-ICALENDAR:VEVENT objects. Grab events from some 'events source' EVENTS where EVENTS is either a list of event structures or a pathname representing a file containing icalendar/vcalendar data."))

(defmethod source-to-vevents ((events-source list))  
  events-source)

;; (defmethod source-to-vevents ((events-source string))
;;   (error "Code this"))

(defmethod source-to-vevents ((events-source pathname))
  (let ((calendar (cl-icalendar:open-vcalendar events-source)))
    ;; ignore other components (alarms, todos, freebusy, ...)
    (calendar->vevents calendar)))

;; START and END are universal times
(defun vevent-begins-in-range-p (vevent start end)
  (let ((dtstart-universal (vevent-dtstart vevent)))
    (and (>= dtstart-universal start)
	 (< dtstart-universal end))))

(defun vevent-description (vevent)
  (let ((description? (first (gethash "DESCRIPTION" (cl-icalendar::property-table vevent)))))
    (if description? (cl-icalendar::property-value description?))))

(defun vevent-dtend (vevent)
  "Return multiple values, an integer representing a universal time and, second, a string indicating whether the value represents a 'DATE' or a 'DATE-TIME'."
  (let* ((dtend-list (gethash "DTEND" (cl-icalendar::property-table vevent)))
	 (dtend? (first dtend-list)))
    (if dtend? 
	(values
	 (cl-icalendar::property-value dtend?)
	 (first (remove "VALUE" (cl-icalendar::property-parameters dtend?) :test #'string=))))))

(defun vevent-dtstart (vevent)
  "Return an integer representing a universal time."
  (let ((dtstart? (first (gethash "DTSTART" (cl-icalendar::property-table vevent))))) 
    (if dtstart? 
	(values
	 (cl-icalendar::property-value dtstart?)
	 (first (remove "VALUE" (cl-icalendar::property-parameters dtstart?) :test #'string=))))))

(defun vevent-dtstart-value-type (vevent)
  "Return an integer representing a universal time."
  (let ((dtstart? (first (gethash "DTSTART" (cl-icalendar::property-table vevent))))) 
    (if dtstart? 
	(first (remove "VALUE" (cl-icalendar::property-parameters dtstart?) :test #'string=)))))

(defun vevent-dtstart-dtend (vevent)
  (multiple-value-bind (startval starttype)
      (vevent-dtstart vevent)
    (multiple-value-bind (endval endtype)
	(vevent-dtend vevent)
      (values startval endval starttype endtype))))

;; return VEVENT if the corresponding DTSTART and DTEND values lie between START and END
(defun vevent-dtstart-dtend-between (vevent start end)
  (multiple-value-bind (dtstart dtend)
      (vevent-dtstart-dtend vevent)
    (let ((start-universal (cl-icalendar::property-value dtstart))
	  (end-universal (cl-icalendar::property-value dtend)))
      (if (and (>= start-universal start)
	       (<= end-universal end))
	  vevent))))

(defun vevent-location (vevent)
  (let ((location? (first (gethash "LOCATION" (cl-icalendar::property-table vevent)))))
    (if location? (cl-icalendar::property-value location?))))

(defun vevent-rrule (vevent)
  (let ((rrule? (first (gethash "RRULE" (cl-icalendar::property-table vevent)))))
    (if rrule? (cl-icalendar::property-value rrule?))))

(defun vevent-summary (vevent)
  (let ((summary? (first (gethash "SUMMARY" (cl-icalendar::property-table vevent)))))
    (if summary? (cl-icalendar::property-value summary?))))

;; START: date/time (universal time) at which calendar should begin
;; END: date/time (universal time) at which calendar should end
(defun vevents-beginning-in-range (start end vevents)
  ;; in the range from start to end, find all
  ;; - discrete events 
  ;; - recurring events with recurrences between start and end
  (alexandria:flatten
   (loop for vevent in vevents
      if (vevent-begins-in-range-p vevent start end)
      collect vevent)))

(defun vevents-in-range-sets (start end vevents)
  "Return, as multiple values, both those in the range and those not in the range. VEVENTS: a list of vevent objects."
  ;; in the range from start to end, find all
  ;; - discrete events 
  ;; - recurring events with recurrences between start and end
  (sort-vevents vevents
		#'(lambda (vevent)
		    (vevent-begins-in-range-p vevent start end))))

;; a vevent overlaps with another vevent if either
;; 1. one has a DTSTART with a DATE-TIME value and the other has a DTSTART with a DATE-TIME value and the two times are identical
;; 2. one has a DTSTART with a DATE-TIME value and the other has both a DTSTART and a DTEND with a DATE-TIME value and the start time of the former vevent lies within the time range associated with the latter event
(defun vevents-overlap-p (vevent1 vevent2)
  (multiple-value-bind (vevent1-dtstart vevent1-dtend vevent1-dtstart-type vevent1-dtend-type)
      (vevent-dtstart-dtend vevent1)
    (multiple-value-bind (vevent2-dtstart vevent2-dtend vevent2-dtstart-type vevent2-dtend-type)
	(vevent-dtstart-dtend vevent2)
      (or 
       ;; if start time for vevent2 lies within time range for vevent1
       (and (string= vevent1-dtstart-type "DATE-TIME") (string= vevent1-dtend-type "DATE-TIME")
	    (and (< vevent2-dtstart vevent1-dtend)
		 (> vevent2-dtstart vevent1-dtstart)))
       ;; if start time for vevent1 lie within time range for vevent2
       (and (string= vevent2-dtstart-type "DATE-TIME") (string= vevent2-dtend-type "DATE-TIME")
	    (and (< vevent1-dtstart vevent2-dtend)
		 (> vevent1-dtstart vevent2-dtstart)))
       (and (string= vevent1-dtstart-type "DATE-TIME") (string= vevent2-dtend-type "DATE-TIME")
	    (= vevent1-dtstart vevent2-dtstart))))))
