(in-package :dcal)

(defvar *evs* nil
  "A list where each member is a list. Each member may have one of two forms:

1. a list with, at the indicated position, the following
    - 0  event label +/- description (either a string or a cons of label and description)
    - 1  days - a single keyword or a list of day keywords
    - 2  start hour
    - 3  a direct or indirect indication of the length of event - either (mins :min) or a number indicating the end time (as the hour of the day)
    - 4  plist
2. a list where the first member is a day keyword and the remaining members each are a list of the form described above but without the days element."
  )

(defun evs-to-events (evs)
  (let ((events nil))
    (map nil
	 #'(lambda (ev)
	     (cond ((keywordp (elt ev 0))
		    (let ((day-keyword (elt ev 0)))
		      (setf events
			    (append events
				    (evs-to-events (mapcar
						    #'(lambda (x)
							(cons (elt x 0)
							      (cons
							       day-keyword
							       (subseq x 1))))
						    (rest ev)))))))
		   (t
		    (assert (numberp (elt ev 2)))
		    (push (ev-to-event ev) events))))
	 evs)
    events))

(defun ev-to-event (ev)
  "Return an event structure."
  (let ((days (elt ev 1))
	(hr-start (elt ev 2))
	(label nil)
	(length nil)	; length of event in minutes
	;; length-spec is either an integer indicating the end-time or
	;; a cons specifying the length of the event (N :min) where N
	;; is the number of minutes
	(length-spec (elt ev 3))
	(description nil)
	(raw-label-description (elt ev 0))
	(plist (subseq ev 4)))
    (cond ((stringp raw-label-description)
	   (setf label raw-label-description))
	  ((consp raw-label-description)
	   (setf label (elt raw-label-description 0))
	   (setf description (elt raw-label-description 1))))
    ;; days
    (cond ((keywordp (elt ev 1))
	   (setf days (list (elt ev 1)))))
    ;; convert length-spec, if necessary to length, in min, of the event
    (setf length
	  (cond ((consp length-spec)
		 ;; only support units of minutes
		 (assert (eq (second length-spec) :min))
		 (first length-spec))
		((numberp length-spec)
		 (* 60.0 (- length-spec hr-start)))))
    (assert (>= length 0))	; sanity check
    (make-event :days days
		:description description
		:label label
		:hr-start hr-start
		:length length
		:fill (getf plist :fill)
		:location (getf plist :location)
		:month (getf plist :month)
		:recurrent (getf plist :recurrent)
		:tag (getf plist :tag)
		:year (getf plist :year)))
  )
