(in-package :dcal) 
;;
;; ALEXANDRIA
(defun flatten (tree)
  "Traverses the tree in order, collecting non-null leaves into a list."
  (let (list)
    (labels ((traverse (subtree)
               (when subtree
                 (if (consp subtree)
                     (progn
                       (traverse (car subtree))
                       (traverse (cdr subtree)))
                     (push subtree list)))))
      (traverse tree))
    (nreverse list)))
;;
;; DAT-CL-UTILS/shared-list.lisp
(defun list-of-integers (n m)
  "Generate a list of integers beginning with N and ending with M"
  (let ((return-value nil))
    (do ((x n (1+ x)))
	((> x m)
	 (reverse return-value))
      (push x return-value))))
;;
;; DAT-CL-UTILS/shared-math.lisp
(defun between (x a b)
  "Return a true value if X is between A and B."
  (if (< a b)
      (between-core x a b)
      (between-core x b a)))
(defun between-core (x a b)
  "A is less than B."
  (and (> x a)
       (< x b)))

(defun to-float (x &optional (errorp t)) 
  "X is an float or a string representation of an float."
  ;; convert string to rational or whatever first...
  (when (stringp x)
    (setf x (read-from-string x))) 
  (cond ((integerp x)
	 (float x))
	((floatp x)
	 x)
	((rationalp x)
	 (float x))
	(errorp
	 (error "argument must represent a float, integer, or rational"))
	(t nil)
	))

;;
;; DCU/shared-string.lisp
(defun list-to-stream (some-list stream &optional (spacer " "))
  "Send content to stream STREAM using the list SOME-LIST where objects in the returned string are separated by string SPACER. Objects in SOME-LIST are rendered with FORMAT ~A

note(s):
  - order retained
  - SPACER does not succeed last item in list
  - clarify differences: at least two other functions in SHARED-STRING have similar functionality:
** this doesn't append spacer to end...
"
  (if (consp some-list)
      (let ((lastitem (car (last some-list)))
	    (len (length some-list)))
	;; everything except last item...
	(dolist (x (subseq some-list 0 (- len 1))) 
	  (if (stringp x)
	      (write-string x stream) 
	      (format stream "~A" x))
	  (write-string spacer stream))
	(if (stringp lastitem)
	    (write-string lastitem stream)
	    (format stream "~A" lastitem)))))
;;
;; DAT-FILE
(defun file-or-directory-exists (pathname)
  "Given pathname or string PATHNAME, return a true value if corresponding file or directory exists. Otherwise return nil."
  ;; Note: some versions of cl-fad file-exists-p and probe-file return true value when an empty string is handed to the function -- cl-fad directory-exists-p also returns a true value with an empty string..."
  (assert (or (stringp pathname)
	      (pathnamep pathname)))
  (let ((stringname (if (stringp pathname)
			pathname
			(namestring pathname)))
	(pathname (if (pathnamep pathname)
		      pathname
		      (pathname pathname))))
    (cond ((string= stringname "")
	   nil)
	  (t
	   (probe-file pathname)))))

(defun file-to-stream (pathname stream)
  "PATHNAME is a string or pathname. If PATHNAME points to a file which exists and is readable, send content of file to stream STREAM. If the file does not exist, return nil. Signal an error if file contains unreadable data. Note(s):
- 'readable' currently means the file only contains objects corresponding to lisp characters. Return value undefined."
  (if (file-or-directory-exists pathname) 
      (let ((file-s (open pathname :direction :input)))
	(do ((x (read-char file-s nil :eof) (read-char file-s nil :eof)))
	    ((eq x :eof))
	  (write-char x stream))
	(close file-s))))

(defun file-to-string (pathname)
  "PATHNAME is a string or pathname. If PATHNAME points to a file which exists and is readable, return a string corresponding to the content of the file. If the file does not exist, return nil. Signal an error if file contains unreadable data. Note(s):
- 'readable' currently means the file only contains objects corresponding to lisp characters"
  (if (file-or-directory-exists pathname)
      (with-output-to-string (string-s nil :element-type 'character)
	(file-to-stream pathname string-s))))

(defun touch (some-path)
  ;;(touch-shell some-path)
  (unless (file-or-directory-exists some-path) 
    (with-open-file (s some-path
		       :direction :output
		       :if-does-not-exist :create)
      nil))
  #+SBCL (SB-POSIX:UTIMES some-path nil (sb-posix:time)) ; thanks, Zach
  #+CCL (CCL::TOUCH some-path)
  #+(not (or SBCL CCL)) (error "SBCL and CCL supported")
  )

;;
;; DAT-TIME/days.lisp
(defparameter *day-abbr3* '("Mon" "Tue" "Wed" "Thu" "Fri" "Sat" "Sun"))

(defun weekend-day-p (universal)
  (> (universal-time-day-of-week universal)
     4))

;;
;; DAT-TIME/iso8601.lisp
(defun iso8601-date-string (universal-time &key day mn yr)
  "Return a string, representing a date, with the form 2010-06-07. UNIVERSAL-TIME, an integer, supersedes current time if UNIVERSAL-TIME is non-negative. DAY, MN, YR supersede UNIVERSAL-TIME or current time."
  (declare (integer universal-time))
  (multiple-value-bind (second minute hour date month year)
      (if (>= universal-time 0) 
	  (decode-universal-time universal-time)
	  (get-decoded-time))
    (declare (ignore second minute hour))
    (let ((d (or day date))
	  (mo (or mn month))
	  (y (or yr year)))
      (format nil "~4,'0D-~2,'0,D-~2,'0,D" y mo d))))
;;
;; DAT-TIME/shared-datetime.lisp
(defparameter *day-in-seconds* 86400)

(defparameter *day-names* '("Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday" "Sunday"))

(defparameter *hour-in-seconds* 3600)

(defparameter *week-in-seconds* 604800)

(defun days-spanned-by (time1 time2)
  "Return an integer. TIME1 is a universal time preceding universal time TIME2." 
  (assert (> time2 time1))
  ;; identify start (first second) of first complete day succeeding time1
  ;; identify end (last second) of first complete day preceding time2
  (let ((first-whole-day-start (start-of-nth-day time1 1))
	(last-whole-day-end (+ (1- *day-in-seconds*) 
			       (start-of-nth-day time2 -1))))
    (cond ((= (- first-whole-day-start last-whole-day-end)
	      1)
	   ;; time1 is exact start of first day and time 2 is exact start of succeeding day
	   2
	   )
	  ((> first-whole-day-start last-whole-day-end)
	   ;; a complete day isn't bracketed by time1 and time2 (e.g., time1 is on 8/22/2013 and time2 is on 8/23/2013)
	   0)
	  (t
	   (let ((number-of-days 
		  (/ (- (1+ last-whole-day-end) 
			first-whole-day-start)
		     *day-in-seconds*
		     )))
	     (if (< time1 first-whole-day-start)
		 (incf number-of-days))
	     (if (> time2 last-whole-day-end)
		 (incf number-of-days))
	     number-of-days)))))

(defun hours-to-seconds (hours)
  "Return an integer."
  ;; don't do this: (* hours 3600)
  ;; ensure answer returned is an integer:
  (round (* hours 3600)))

(defun seconds-to-hours (seconds)
  (/ seconds 3600))

;; HOUR is an integer between 0 and 23
(defun us-hour (hour) 
  (cond ((> hour 12)
	 (- hour 12))
	((= hour 0)
	 12)
	(t hour)))
;;
;; DAT-TIME/universal.lisp
(defun a-week-ago (time)
  "Return universal time corresponding to one week before the time TIME."
  (- time *week-in-seconds*))

(defgeneric adjust-to (time time-component x))

(defmethod adjust-to (time (time-component (eql :hr)) x)
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time time) 
    (declare (ignore hour))
    (encode-universal-time second minute x date month year)))

;; TIME is a universal time; TIME-COMPONENT is a keyword; X is the corresponding minimum value for that slot in the returned universal time equivalent
(defgeneric adjust-to-at-least (time time-component x))

(defmethod adjust-to-at-least (time (time-component (eql :hr)) x)
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time time)
    (if (>= hour x)
	(encode-universal-time second minute hour date month year)
	(encode-universal-time 0 0 x date month year))))

(defgeneric adjust-to-no-more-than (time time-component x))

(defmethod adjust-to-no-more-than (time (time-component (eql :hr)) x)
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time time)
    (if (<= hour x)
	(encode-universal-time second minute hour date month year)
	(encode-universal-time 0 0 x date month year))))

(defun end-of-day (time)
  "Return the universal time representing the end of the day containing INITIAL-TIME."
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time time)
    (declare (ignore second minute hour))
    (encode-universal-time 59 59 23 date month year)))

;; test: DCAL> (hours-in-range 3569871654 3569903999) -> (15 16 17 18 19 20 21 22 23)

;; return ascending list of universal times corresponding to hours within range START and END
(defun hours-in-range (start end)
  (let ((accum nil))
    (do ((time end (- time 3600)))
	((< time start))
      (push (universal-time-hr time) accum))
    accum))

(defun next-month (month year)
  "Given month MONTH in year YEAR, return as multiple values the month and year value corresponding to the next month."
  (declare (fixnum month year))
  (assert (and (<= month 12) (> month 0)))
  (cond ((= 12 month)
	 (values 1 (1+ year)))
	(t ; (< month 12)
	 (values (1+ month) year))))

;; FIXME: put in DCU
;; DCAL> (pairwise-comparison-indices '(0 1 2 99 3 99 2 2) #'=)
;; (NIL NIL (7 6) (5) NIL (3) (7 2) (6 2))
(defun pairwise-comparison-indices (things test)
  "TEST is a funcallable object which accepts, as arguments, two members of THINGS and returns a true value, indicating 'overlap'. Return a list of lists. Each sublist is a list of integers corresponding to positions of items in THINGS which 'match' when TEST is applied. TEST is not evaluated with an object in THINGS relative to itself (comparisons are only made with other members of THINGS)."
  ;; mapcar is nice but objects overlap with themselves... kludge: use indices
  (let ((indices (list-of-integers 0 (1- (length things)))))
    (mapcar #'(lambda (index)
		(let ((thing (nth index things))
		      (this-thing-indices nil))
		  ;; look for overlap
		  (dotimes (x (length things))
		    (let ((comparison-thing (nth x things)))		    
		      ;; ignore overlap at boundaries  - comparing 12-13 to 13-14
		      ;; ignore identical events
		      (if (and (not (= index x))
			       (funcall test thing comparison-thing))
			  (push x this-thing-indices))))
		  this-thing-indices))
	    indices)))

(defun start-of-day (time)
  "Return the universal time representing the start of the day containing INITIAL-TIME."
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time time)
    (declare (ignore second minute hour))
    (encode-universal-time 0 0 0 date month year)))

(defun start-of-next-month (time)
  "Return a universal time."
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time time)
    (declare (ignore second minute hour date))
    (multiple-value-bind (next-month next-year)
	(next-month month year)
      (encode-universal-time 0 0 0 1 next-month next-year))))

(defun start-of-nth-day (initial-time day-index)
  "Return the universal time representing the start of the nth day relative to universal time INITIAL-TIME where DAY-INDEX is an integer representing the nth day where 0 corresponds to the day containing INITIAL-TIME."
  ;; DAY-LENGTH = (* 24 60 60) = 86400 s
  (start-of-day (+ (* 86400 day-index) initial-time)))

;; return a true value if universal time TIME is in the day associated with universal time DAYTIME
(defun time-in-day-p (time daytime)
  (time-in-range-p time (start-of-day daytime) (end-of-day daytime)))

(defun time-in-range-p (time start end)
  (and (>= time start)
       (<= time end)))

(defun universal-time-date (universal-time)
  (declare (integer universal-time))
  (multiple-value-bind (second minute hour date month year day-of-week)
      (decode-universal-time universal-time)
    (declare (ignore second minute hour month day-of-week year))
    date))

(defun universal-time-day-abbr3 (universal-time)
  (declare (integer universal-time))
  (nth (universal-time-day-of-week universal-time) *day-abbr3*))

(defun universal-time-day-name (universal-time)
  (declare (integer universal-time))
  (nth (universal-time-day-of-week universal-time) *day-names*))

(defun universal-time-day-of-week (universal-time)
  (declare (integer universal-time))
  (multiple-value-bind (second minute hour date month year day-of-week)
      (decode-universal-time universal-time)
    (declare (ignore second minute hour date month year))
    day-of-week))

(defun universal-time-hr (universal-time)
  "Return an integer"
  (declare (integer universal-time))
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time universal-time)
    (declare (ignore second minute date month year))
    hour))

(defun universal-time-hr-exact (universal-time)
  "Return a value between 0 and 24."
  (declare (integer universal-time))
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time universal-time)
    (declare (ignore date month year))
    (+ hour (/ minute 60) (/ second *hour-in-seconds*))))

(defun universal-time-min (universal-time)
  "Return an integer."
  (declare (integer universal-time))
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time universal-time)
    (declare (ignore second hour date month year))
    minute))

(defun universal-time-month (universal-time)
  (declare (integer universal-time))
  (multiple-value-bind (second minute hour date month year day-of-week)
      (decode-universal-time universal-time)
    (declare (ignore second minute hour date day-of-week year))
    month))

(defun universal-time-p (x)
  (and (integerp x) (>= x 0)))

(deftype universal-time ()
  '(satisfies universal-time-p))

(defmacro with-enclosing (start end stream &body body)
  `(progn (write-string ,start ,stream)
	  ,@body
	  (write-string ,end ,stream)))

